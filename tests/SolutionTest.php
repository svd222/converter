<?php
namespace Test;

use PHPUnit\Framework\TestCase;
use App\Solution;

class SolutionTest extends TestCase
{
    protected $roman = '';

    protected $incorrectRoman = '';

    public function setUp(): void
    {
        $this->roman = 'MCMLXXXVIII';
        $this->incorrectRoman = 'MCMLXXXXVIII';
        parent::setUp();
    }

    public function testOutOfRangeException()
    {
        $this->expectException(\OutOfRangeException::class);

        $solution = new Solution;
        $solution->romanToInt($this->incorrectRoman);
    }

    public function testEqualsRomanArab()
    {
        $solution = new Solution;
        $arab = $solution->romanToInt($this->roman);
        $this->assertEquals(188, $arab);
    }
}