<?php
namespace App;

use OutOfRangeException;

/**
 * Class Solution
 * @package App
 */
class Solution
{
    private $romanDigits = [
        ['I', 1],
        ['V', 5],
        ['X', 10],
        ['L', 50],
        ['C', 100],
        ['D', 500],
        ['M', 1000],
    ];

    /**
     * @param string $roman
     * @return int
     */
    function romanToInt(string $roman): int
    {
        $sum = 0;
        $count = mb_strlen($roman);
        $lastDigits = [];
        for($i = 0; $i < $count; $i++) {
            $current = $roman[$i];
            $next = $roman[$i + 1] ?? '';
            $arab = $this->getArab($current, $next);
            $lastDigits = $this->checkForBounds($lastDigits, $arab);
            $sum += $arab;
            if ($arab < 0) {
                $i++;
            }
        }
        return $sum;
    }

    /**
     * @param string $current
     * @param string $next
     * @return int
     */
    public function getArab(string $current, string $next): int
    {
        $currentIndex = $this->getIndexByRoman($current);
        $nextIndex = $this->getIndexByRoman($next);
        $index = '';
        if ($nextIndex) {
            $index = $currentIndex <=> $nextIndex;
        }
        if ($index !== '') {
            if ($index < 0) {
                $index = [$currentIndex, $nextIndex];
            } else {
                $index = $currentIndex;
            }
        } else {
            $index = $currentIndex;
        }
        return $this->getArabByIndex($index);
    }

    /**
     * @param $roman
     * @return int
     */
    protected function getIndexByRoman($roman)
    {
        foreach ($this->romanDigits as $k => $v) {
            if ($v[0] == $roman) {
                return $k;
            }
        }
        return '';
    }

    /**
     * Returns arab digit by current position is search string
     *
     * @param $index
     * @return mixed
     */
    protected function getArabByIndex($index)
    {
        if (!is_array($index)) {
            return $this->romanDigits[$index][1];
        } else {
            return $this->romanDigits[$index[0]][1] - $this->romanDigits[$index[1]][1];
        }
    }

    /**
     * Auxuliary method for check count of repeating rome digits
     *
     * @param $lastDigits
     * @param $arab
     * @throws OutOfRangeException
     * @return mixed
     */
    protected function checkForBounds($lastDigits, $arab) {
        if (!count($lastDigits)) {
            $lastDigits[] = $arab;
        } else {
            if ($lastDigits[0] == $arab) {
                $lastDigits[] = $arab;
            } else {
                $lastDigits = [];
                $lastDigits[0] = $arab;
            }
        }
        if (count($lastDigits) > 3) {
            throw new OutOfRangeException('Rome digits should not be repeat more than 3 times consecutive');
        }
        return $lastDigits;
    }
}
